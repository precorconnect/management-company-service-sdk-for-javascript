## Description
Precor Connect management company service SDK for javascript.

## Features

## Setup

**install via jspm**  
```shell
jspm install management-company-service-sdk=bitbucket:precorconnect/management-company-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import ManagementCompanyServiceSdk,{ManagementCompanyServiceSdkConfig} from 'management-segment-service-sdk'

const managementCompanyServiceSdkConfig =
    new ManagementCompanyServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const managementCompanyServiceSdk =
    new ManagementCompanyServiceSdk(
        managementCompanyServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```