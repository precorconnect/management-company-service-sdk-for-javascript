import  ManagementCompanyServiceSdkConfig from  '../../src/managementCompanyServiceSdkConfig';

export default {
    managementCompanyServiceSdkConfig: new ManagementCompanyServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    existingCustomerSegmentId: 1
};
