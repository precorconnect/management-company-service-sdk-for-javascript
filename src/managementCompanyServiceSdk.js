import DiContainer from './diContainer';
import ManagementCompanyServiceSdkConfig from './managementCompanyServiceSdkConfig';
import ListManagementCompaniesFeature from './listManagementCompaniesFeature';

/**
 * @class {ManagementCompanyServiceSdk}
 */

export default class ManagementCompanyServiceSdk{

    _diContainer:DiContainer;

    constructor(config:ManagementCompanyServiceSdkConfig) {
       this._diContainer =  new DiContainer(config)
    }

    listManagementCompanies(accessToken:string):Promise<Array> {
        return this
            ._diContainer
            .get(ListManagementCompaniesFeature)
            .execute(accessToken)
    }

}
