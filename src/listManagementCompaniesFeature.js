import {inject} from 'aurelia-dependency-injection';
import ManagementCompanyServiceSdkConfig from './managementCompanyServiceSdkConfig';
import ManagementCompanySynopsisView from './managementCompanySynopsisView';
import {HttpClient} from 'aurelia-http-client';

@inject(ManagementCompanyServiceSdkConfig,HttpClient)
class ListManagementCompaniesFeature{

    _config:ManagementCompanyServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:ManagementCompanyServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * list Management Companies
     * @param {string} accessToken
     */
    execute(accessToken:string):Promise<ManagementCompanySynopsisView[]>
    {
        return this._httpClient
            .createRequest(`/management-companies/`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response);
    }

}

export  default ListManagementCompaniesFeature;