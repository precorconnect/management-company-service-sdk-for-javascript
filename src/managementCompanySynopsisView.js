/**
 * @class {ManagementCompanySynopsisView}
 */
export default class ManagementCompanySynopsisView{

    _id;

    _name;

    /**
     *
     * @param {string} id
     * @param {string} name
     */
    constructor(id,name){

        if(!id){
           throw new TypeError('id required');
        }
        this._id = id;

        if(!name){
            throw new TypeError('name required');
        }
        this._name = name;

    }


    /**
     * @returns {string} id
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string} name
     */
    get name():string {
        return this._name;
    }

}

